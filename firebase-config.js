// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getDocs, getFirestore } from "@firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBUiwONoj292nN6u1_G8jIuuj18j7PeQKw",
  authDomain: "employee-attendance-90644.firebaseapp.com",
  projectId: "employee-attendance-90644",
  storageBucket: "employee-attendance-90644.appspot.com",
  messagingSenderId: "1052039688490",
  appId: "1:1052039688490:web:d09329a16000fc14816e53",
  measurementId: "G - 7SK8P8V1L4"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
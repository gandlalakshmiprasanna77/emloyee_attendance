const Requests = "/admin/leaveRequests", AdminDashBoard = "/admin/adminDashboard", EmployeeDashBoard = "/employee/employeeDashboard", HomePage = "/";
export const getAdminDashBoardUrl = () => AdminDashBoard;
export const gerEmployeeDashBoardUrl = () => EmployeeDashBoard;
export const getHomePageUrl = () => HomePage;
export const getLeaveUrl = () => Leave;
export const leaveRequestsUrl = () => Requests;
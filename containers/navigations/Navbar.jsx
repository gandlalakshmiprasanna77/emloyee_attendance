import {
  getAdminDashBoardUrl,
  gerEmployeeDashBoardUrl,
  leaveRequestsUrl,
} from "../../constants/AppUrl";
import Link from "next/link";
import { useSelector } from "react-redux";
import { useState } from "react";
import Modal from "../../Components/graphModal";
const Navbar = (props) => {
  const [showModal, setshowModal] = useState(false);
  const id = useSelector((state) => state.employee.id);
  const is_employee_loggedin = useSelector((state) => state.employee.loggedIn);
  const is_admin_loggedin = useSelector((state) => state.admin.loggedIn);
  const openModal = () => {
    setshowModal(true);
  };
  const removeModal = () => {
    setshowModal(!showModal);
  };

  return (
    <nav
      className="navbar navbar-expand-lg navbar-light"
      style={{ backgroundColor: "#6f42c1" }}
    >
      {!is_admin_loggedin && !is_employee_loggedin ? (
        <a className="navbar-brand text-light mb-3 mt-3" href="#">
          Health Signz Technology Private Limited
        </a>
      ) : (
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            {is_admin_loggedin ? (
              <Link href={getAdminDashBoardUrl()}>
                <a className="nav-link text-light fs-5">AttendanceForm</a>
              </Link>
            ) : (
              <Link href={`/${id}`}>
                <a className="nav-link text-light fs-5">Attendance info</a>
              </Link>
            )}
          </li>
          {is_employee_loggedin ? (
            <li className="nav-item navleft">
              <Link href={gerEmployeeDashBoardUrl()}>
                <a
                  className="nav-link text-light navright"
                  style={{ fontSize: "20px" }}
                >
                  Apply Leave
                </a>
              </Link>
            </li>
          ) : (
            <>
              <li className="nav-item navleft">
                <Link href={leaveRequestsUrl()}>
                  <a
                    className="nav-link text-light "
                    style={{ fontSize: "20px" }}
                  >
                    Leave Request
                  </a>
                </Link>
              </li>
              <li className="nav-item navleft adnavright" onClick={openModal}>
                <a
                  className="nav-link text-light"
                  style={{ fontSize: "20px", cursor: "pointer" }}
                >
                  Leave Graph
                </a>
              </li>
              {showModal ? (
                <Modal popUpModal={openModal} closeModal={removeModal} />
              ) : null}
            </>
          )}
          <li className="nav-item">
            <Link href="/logout">
              <a className="nav-link text-light btn btn-info btn-lg">Log out</a>
            </Link>
          </li>
        </ul>
      )}
    </nav>
  );
};

export default Navbar;

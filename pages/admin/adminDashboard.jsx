import Wrapper from "../../Components/shared/wrapper";
import Form from "../../Components/Form/form";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { leaveRequests } from "../../store/actions/adminActions";
const adminDashboard = ({ data }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    const employee_requests = data.requests.documents.map((request) => {
      request.fields.name = {};
      request.fields.name.stringValue = request.name;
      return request.fields;
    });
    dispatch(leaveRequests(employee_requests));
  }, []);
  return (
    <Wrapper>
      <Form />
    </Wrapper>
  );
};
export const getStaticProps = async () => {
  try {
    const resp = await fetch(
      `https://firestore.googleapis.com/v1/projects/employee-attendance-90644/databases/(default)/documents/Leave`
    );
    var requests = await resp.json();
    return {
      props: {
        data: { requests },
      },
      revalidate: 1,
    };
  } catch (error) {
    console.log(error);
  }
};

export default adminDashboard;

import React from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { clearEmpCredentials } from "../store/actions/employeeActions";
import { clearAdminCredentials } from "../store/actions/adminActions";
import { useSelector } from "react-redux";
const logout = () => {
    const dispatch = useDispatch()
    const router = useRouter()
    const employeeLoggedIn = useSelector((state) => state.employee.loggedIn)
    useEffect(() => {
        console.log(employeeLoggedIn)
        if (employeeLoggedIn === true) {
            dispatch(clearEmpCredentials())
        }
        else {
            dispatch(clearAdminCredentials())
        }
        router.push("/")
    })
    return (<></>)
}
export default logout;
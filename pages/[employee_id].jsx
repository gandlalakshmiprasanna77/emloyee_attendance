import React from "react";
import { useEffect } from "react";
import { attendanceData } from "../store/actions/adminActions";
import { useDispatch } from "react-redux";
import Wrapper from "../Components/shared/wrapper";
import Attendance from "../Components/attendance";
import { useRouter } from "next/router";
const employeeAttendance = ({ data }) => {
  const router = useRouter();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(attendanceData(data));
  }, []);
  if (router.isFallback && !data) {
    return <div>loading ...</div>;
  }
  return (
    <React.Fragment>
      <Wrapper>
        <Attendance />
      </Wrapper>
    </React.Fragment>
  );
};
export const getStaticPaths = async () => {
  try {
    const response = await fetch(
      `https://firestore.googleapis.com/v1/projects/employee-attendance-90644/databases/(default)/documents/EmployeeLoginDetails`
    );
    var empdata = await response.json();
    const employees = empdata.documents;
    const credentials = employees.map((employee) => {
      const id = employee.fields.employeeid.stringValue;
      return id;
    });
    return {
      paths: credentials.map((user) => ({
        params: { employee_id: user },
      })),
      fallback: true,
    };
  } catch (error) {
    console.log(error);
  }
};
export const getStaticProps = async (context) => {
  try {
    const resp = await fetch(
      `https://firestore.googleapis.com/v1/projects/employee-attendance-90644/databases/(default)/documents/Attendance/${context.params.employee_id}`
    );
    var attendance = await resp.json();
    const data = attendance.fields;
    return {
      props: {
        data: { data },
      },
      revalidate: 86400,
    };
  } catch (error) {
    console.log(error);
  }
};
export default employeeAttendance;

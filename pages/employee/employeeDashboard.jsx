import Wrapper from "../../Components/shared/wrapper";
import LeaveForm from "../../Components/leaveApplicationForm";
const employeeDashboard = () => {
  return (
    <Wrapper>
      <LeaveForm />
    </Wrapper>
  );
};
export default employeeDashboard;

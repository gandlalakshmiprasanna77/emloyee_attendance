import Wrapper from "../Components/shared/wrapper";
import Form from "../Components/Form/loginForm";
import { employeeDetails } from "../store/actions/employeeActions";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";
const index = () => {
  const dispatch = useDispatch();
  const [data, setdata] = useState({})
  useEffect(() => {
    (async () => {
      try {
        var resp = await fetch(
          `https://firestore.googleapis.com/v1/projects/employee-attendance-90644/databases/(default)/documents/AdminLoginDetails`
        );
        var admin = await resp.json();
        if (resp.status === 200) {
          const data = {}
          data.id = admin.documents[0].fields.Id.stringValue;
          data.password = admin.documents[0].fields.password.stringValue;
          setdata(data)
        }
      } catch (error) {
        console.log(error);
      }

    })();
    dispatch(employeeDetails());
  }, []);
  return (
    <Wrapper>
      <Form admindata={data} />
    </Wrapper>
  );
};
export default index;

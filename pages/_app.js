import '../styles/styles.css'
import { wrapper, store } from "../store/store";
import { Provider } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import { StyledEngineProvider } from '@mui/material/styles';

function MyApp({ Component, pageProps }) {
    return (
        <>
            <StyledEngineProvider injectFirst>
            <Provider store={store}>
                <Component {...pageProps} />
            </Provider>
            </StyledEngineProvider>
        </>
    )
}
export default wrapper.withRedux(MyApp);

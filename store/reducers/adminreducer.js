import { Attendance, Leave, AdminLoginAction, AdminLogoutAction } from "../actions/ActionTypes";
const initialState = {
  attendance: {},
  leave: [],
  id: "",
  loggedIn: false
};

const adminReducer = (state = initialState, action) => {
  switch (action.type[0]) {
    case Attendance:
      return {
        ...state,
        attendance: action.payload,
      };
    case Leave:
      return {
        ...state,
        leave: action.payload
      }
    case AdminLoginAction:
      return {
        ...state,
        id: action.payload,
        loggedIn: true
      }
    case AdminLogoutAction:
      return {
        ...state,
        id: "",
        loggedIn: false
      }
    default:
      return state;
  }
};

export default adminReducer;

import { combineReducers } from "@reduxjs/toolkit";
import AdminReducer from "./adminreducer"
import EmployeeReducer from "./employeereducer"
const reducer = combineReducers({
    admin: AdminReducer,
    employee: EmployeeReducer
});
export default reducer;
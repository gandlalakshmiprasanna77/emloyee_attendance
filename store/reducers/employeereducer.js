import { EmployeeCredentials, empId, clearempcredentials } from "../actions/ActionTypes";
const initialState = {
  employees: [],
  id: "",
  name: "",
  loggedIn: false,
};

const employeeReducer = (state = initialState, action) => {
  switch (action.type[0]) {
    case EmployeeCredentials:
      return {
        ...state,
        employees: action.payload
      };
    case empId:
      return {
        ...state,
        id: action.payload[0],
        name: action.payload[1],
        loggedIn: true,
      };
    case clearempcredentials:
      return {
        ...state,
        id: "",
        name: "",
        loggedIn: false
      }
    default:
      return state;
  }
};

export default employeeReducer;

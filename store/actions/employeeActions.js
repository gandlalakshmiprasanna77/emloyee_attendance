import { EmployeeCredentials, empId, clearempcredentials } from "./ActionTypes";

export const employeeDetails = (credentials) => (dispatch) => {
  const getuser = async () => {
    try {
      var response = await fetch(
        `https://firestore.googleapis.com/v1/projects/employee-attendance-90644/databases/(default)/documents/EmployeeLoginDetails`
      );
      var empdata = await response.json();
      const employees = empdata.documents;
      const credentials = employees.map((employee) => {
        const user = {};
        user.id = employee.fields.employeeid.stringValue;
        user.name = employee.fields.name.stringValue;
        user.password = employee.fields.password.stringValue;
        return user;
      });
      dispatch({
        type: [EmployeeCredentials],
        payload: credentials,
      });
    } catch (error) {
      console.log(error);
    }
  };
  getuser();
};

export const fetchingEmpId = (id, name) => (dispatch) => {
  dispatch({
    type: [empId],
    payload: [id, name],
  });
};
export const clearEmpCredentials = () => (dispatch) => {
  dispatch({
    type: [clearempcredentials],
    payload: null,
  });
};

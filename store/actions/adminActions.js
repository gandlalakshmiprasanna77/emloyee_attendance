import { Attendance, Leave, AdminLoginAction, AdminLogoutAction } from "./ActionTypes";

export const attendanceData = (attendancedata) => dispatch => {
    const month_keys = Object.keys(attendancedata.data)
    const dicti_data = {}
    month_keys.forEach(month => {
        dicti_data[month] = attendancedata.data[month].mapValue.fields
    })

    dispatch({
        type: [Attendance],
        payload: dicti_data
    })
}
export const leaveRequests = (leaveinformation) => dispatch => {
    console.log(leaveinformation)
    dispatch({
        type: [Leave],
        payload: leaveinformation
    })
}
export const fetchingAdminData = (id) => dispatch => {
    dispatch({
        type: [AdminLoginAction],
        payload: id
    })
}
export const clearAdminCredentials = () => dispatch => {
    dispatch({
        type: [AdminLogoutAction],
        payload: null
    })
}



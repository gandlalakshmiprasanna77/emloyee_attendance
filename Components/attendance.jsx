import { useState } from "react";
import { useSelector } from "react-redux";
import AttendanceInfo from "./fetchingAttendanceInfo";

const attendance = () => {
  const [month, setmonth] = useState(new Date().getMonth());
  const [year, setyear] = useState(new Date().getFullYear());
  const attendancedata = useSelector((state) => state.admin.attendance);
  let months = {
    0: "jan",
    1: "feb",
    2: "mar",
    3: "apr",
    4: "may",
    5: "jun",
    6: "july",
    7: "aug",
    8: "sep",
    9: "oct",
    10: "nov",
    11: "dec",
  };
  var date = months[month] + year;

  const handlePrev = () => {
    if (month === 0) {
      setmonth(11);
      setyear(year - 1);
    } else {
      setmonth(month - 1);
    }
  };
  const handleNext = () => {
    if (month === 11) {
      setmonth(0);
      setyear(year + 1);
    } else {
      setmonth(month + 1);
    }
  };
  return (
    <div
      style={{
        backgroundColor: "#f7f7f7",
        margin: "auto",
        marginTop: "5%",
        width: "50%",
      }}
    >
      <div
        className=" m-auto bg-white w-100"
        style={{ boxShadow: " 0 0 5px 0 rgba(0,0,0,0.2)" }}
      >
        <div className="d-flex border-0">
          <div>
            <button
              className="btn btn-sm text-secondary align-item-left fs-5"
              onClick={handlePrev}
            >
              Prev
            </button>
          </div>
          <span
            className="d-flex 
            flex-row 
            justify-content-center 
            align-items-center 
            mt-0 
            mb-0 flex-grow-1"
            style={{ fontSize: "20px" }}
          >
            {date}
          </span>
          <div>
            <button
              className="btn btn-sm text-secondary align-item-right fs-5"
              onClick={handleNext}
            >
              next
            </button>
          </div>
        </div>
        <AttendanceInfo current_month={month} current_year={year} />
      </div>
    </div>
  );
};
export default attendance;

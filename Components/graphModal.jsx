import LineChart from "./lineChart";
import { Modal } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useState } from "react";
import { useEffect } from "react";

const graphModal = (props) => {
  const [xValues, setxValues] = useState([]);
  const [yValues, setyValues] = useState([]);
  const [chartData, setchartData] = useState({
    labels: ["Mon", "Tue", "Wed", "Thu", "Fri"],
    datasets: [
      {
        label: "leaves count",
        data: yValues,
        backgroundColor: [
          "rgba(75,192,192,1)",
          "#ecf0f1",
          "#50AF95",
          "#f3ba2f",
          "#2a71d0",
        ],
        borderColor: "black",
        borderWidth: 2,
      },
    ],
    options: {
      scales: {
        y: {
          beginAtZero: true,
        },
      },
    },
  });
  const employee_requests = useSelector((state) => state.admin.leave);
  const { popUpModal, closeModal } = props;
  var leaves_data;
  leaves_data = employee_requests.filter((request) => {
    return request.status.stringValue === "Accepted";
  });

  useEffect(() => {
    console.log("hello parent");
    var today = new Date();
    let date =
      today.getFullYear() +
      "-" +
      (today.getMonth() + 1) +
      "-" +
      today.getDate();
    let whichday = new Date(date).getDay();
    countingLeaves(today, whichday);
  }, []);
  const countingLeaves = (date, whichday) => {
    let xValues = [];
    let yValues = [];
    for (let i = 1; i <= 5; i++) {
      let diff = whichday - i;
      let a = new Date(date);
      a.setDate(a.getDate() - diff);
      var nextdate =
        a.getFullYear() +
        "-" +
        (parseInt(a.getMonth()) + 1) +
        "-" +
        a.getDate();
      xValues.push(nextdate);
    }
    xValues.forEach((ele) => {
      let count = 0;
      let d3 = ele.split("-");
      let weekday = new Date(d3[0], parseInt(d3[1]) - 1, d3[2]);
      leaves_data.forEach((element, index) => {
        let d1 = element.fromDate.stringValue.split("-");
        let d2 = element.toDate.stringValue.split("-");
        let fromDate = new Date(d1[0], parseInt(d1[1]) - 1, d1[2]);
        let toDate = new Date(d2[0], parseInt(d2[1]) - 1, d2[2]);
        if (weekday >= fromDate && weekday <= toDate) {
          count++;
        }
      });
      yValues.push(count);
    });
    let data = {
      labels: ["Mon", "Tue", "Wed", "Thu", "Fri"],
      datasets: [
        {
          label: "leaves count",
          data: yValues,
          backgroundColor: [
            "rgba(75,192,192,1)",
            "#ecf0f1",
            "#50AF95",
            "#f3ba2f",
            "#2a71d0",
          ],
          borderColor: "black",
          borderWidth: 2,
        },
      ],
    };
    console.log(xValues, yValues);
    setxValues(xValues);
    setyValues(yValues);
    setchartData(data);
    //return [xValues, yValues];
  };
  const prevWeekGraph = () => {
    let date_list = xValues[0].split("-");
    let present_date = new Date(
      date_list[0],
      parseInt(date_list[1]) - 1,
      date_list[2]
    );
    present_date.setDate(present_date.getDate() - 2);
    countingLeaves(present_date, 6);
  };
  const nextWeekGraph = () => {
    let date_list = xValues[4].split("-");
    let present_date = new Date(
      date_list[0],
      parseInt(date_list[1]) - 1,
      date_list[2]
    );
    present_date.setDate(present_date.getDate() + 2);
    countingLeaves(present_date, 0);
  };
  return (
    <Modal
      show={popUpModal}
      onHide={closeModal}
      size="lg"
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title>Leave Graph</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="d-flex border-0">
          <div>
            <button
              className="btn btn-sm text-primary align-item-left fs-5"
              style={{ borderBottom: "1px solid #cbd5e1" }}
              onClick={prevWeekGraph}
            >
              Prev
            </button>
          </div>
          <span
            className="d-flex 
            flex-row 
            justify-content-center 
            align-items-center 
            mt-0 
            mb-0 flex-grow-1"
            style={{ fontSize: "20px" }}
          >
            {xValues[0]} to {xValues[4]}
          </span>
          <div>
            <button
              className="btn btn-sm text-primary align-item-right fs-5"
              style={{ borderBottom: "1px solid #cbd5e1" }}
              onClick={nextWeekGraph}
            >
              next
            </button>
          </div>
        </div>
        <div
          style={{
            width: "500px",
            margin: "auto",
            marginTop: "30px",
            marginBottom: "30px",
          }}
        >
          <LineChart chartData={chartData} />
        </div>
      </Modal.Body>
    </Modal>
  );
};
export default graphModal;

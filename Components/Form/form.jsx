import { useState } from "react";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";

const form = () => {
  const [employeeId, setemployeeId] = useState("");
  const [error, seterror] = useState(false);
  const [errorMsg, seterrorMsg] = useState("");
  const employees = useSelector((state) => state.employee.employees);
  const employee_ids = employees.map((employee) => employee.id);
  const router = useRouter();
  const error_msg1 = "id is not existed";
  const error_msg2 = "enter the id";

  const handleSubmit = (e) => {
    e.preventDefault();
    if (employee_ids.indexOf(employeeId) > -1) {
      router.push(`/${employeeId}`);
    } else if (employeeId !== "") {
      seterrorMsg(error_msg1);
      seterror(true);
    } else {
      seterrorMsg(error_msg2);
      seterror(true);
    }
  };
  return (
    <form onSubmit={handleSubmit}>
      <div
        className="px-3 fw-bold"
        style={{
          width: "300px",
          borderRadius: "5px",
          border: "3px solid black",
          fontFamily: "sans-serif",
          margin: "auto",
          marginTop: "60px",
        }}
      >
        {error === true ? (
          <div
            className="d-flex h-auto mb-3 flex-column ps-4 pe-4 pt-3 pb-3 mt-3"
            style={{ backgroundColor: "#fff7eb", borderRadius: "5px" }}
          >
            <div>{errorMsg}</div>
          </div>
        ) : (
          <></>
        )}

        <div className="mt-3">
          <div>
            <label
              className=""
              style={{ fontSize: "18px" }}
              htmlFor="namedInput"
            >
              Employee Id:
            </label>
          </div>
          <div className="mt-3">
            <input
              style={{ width: "100%", height: "2rem" }}
              className=""
              id="namedInput"
              type="text"
              name="name"
              placeholder="Enter the id"
              value={employeeId}
              onChange={(e) => setemployeeId(e.target.value)}
            />
          </div>
        </div>
        <div className="d-flex flex-row justify-content-around mt-4 mb-4">
          <div>
            <input
              type="submit"
              className="text-light btn btn-primary"
              value="submit"
            />
          </div>
          <div>
            <button
              type="button"
              className="text-light border-0 btn btn-primary"
              style={{ borderRadius: "2px" }}
              onClick={(e) => setemployeeId("")}
            >
              cancel
            </button>
          </div>
        </div>
      </div>
    </form>
  );
};
export default form;

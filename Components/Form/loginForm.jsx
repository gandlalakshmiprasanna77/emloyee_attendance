import styles from "../../styles/loginForm.module.scss";
import { useState } from "react";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import FormLabel from "@mui/material/FormLabel";
import FormControlLabel from "@mui/material/FormControlLabel";
import RadioGroup from "@mui/material/RadioGroup";
import Radio from "@mui/material/Radio";
import { useDispatch } from "react-redux";
import { fetchingEmpId } from "../../store/actions/employeeActions";
import { fetchingAdminData } from "../../store/actions/adminActions";

const loginForm = (props) => {
  const [error, seterror] = useState(false);
  const [userId, setuserId] = useState("");
  const [loginAs, setloginAs] = useState("");
  const [userPassword, setuserPassword] = useState(0);
  const id = props.admindata.id;
  const password = props.admindata.password;
  const employeeCredentials = useSelector((state) => state.employee.employees);
  const router = useRouter();
  const dispatch = useDispatch();
  const [errorMsg, seterrorMsg] = useState("");
  const errormsg1 = "id and password is required";
  const errormsg2 = "one of the id and password are wrong";
  console.log(props.admindata.id);
  const validation = (id, password, path, loginAs) => {
    if (
      userId === id &&
      userPassword === password &&
      userId !== "" &&
      userPassword !== 0
    ) {
      if (loginAs === "Admin") {
        dispatch(fetchingAdminData(props.admindata.id));
      }
      router.push(path);
    } else if (userId === "" || userPassword === 0) {
      seterrorMsg(errormsg1);
      seterror(true);
    } else {
      seterror(true);
      seterrorMsg(errormsg2);
    }
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (loginAs === "Admin") {
      const path = "admin/adminDashboard";
      validation(id, password, path, loginAs);
      console.log(loginAs, id, password);
    } else {
      const employee = employeeCredentials.filter((employee) => {
        return employee.id === userId && employee.password === userPassword;
      });
      const path = "employee/employeeDashboard";
      if (employee.length !== 0) {
        dispatch(fetchingEmpId(userId, employee[0].name));
        validation(employee[0].id, employee[0].password, path, loginAs);
      } else {
        validation(null, null, null);
      }
    }
  };
  return (
    <div className={styles.loginOutbox}>
      <header
        className="mb-4 text-center fw-bold fs-2"
        style={{
          boxSizing: "border-box",
          border: "0 solid",
          fontFamily: "sans-serif",
        }}
      >
        <div>Hello there</div>
      </header>
      <div className="d-flex flex-column">
        <form onSubmit={handleSubmit}>
          {error === true ? (
            <div
              className="d-flex h-auto mb-3 flex-column ps-4 pe-4 pt-3 pb-3"
              style={{ backgroundColor: "#fff7eb", borderRadius: "5px" }}
            >
              <div>{errorMsg}</div>
            </div>
          ) : (
            <></>
          )}
          <div className={styles.formgroup}>
            <label className={styles.labelstyle} htmlFor="namedInput">
              Login Id:
            </label>
            <div>
              <input
                className={styles.inputcontainer}
                id="namedInput"
                type="text"
                name="name"
                placeholder="User Name"
                onChange={(e) => setuserId(e.target.value)}
              />
            </div>
          </div>
          <div className={styles.formgroup}>
            <label className={styles.labelstyle} htmlFor="password">
              password:
            </label>
            <div>
              <input
                className={styles.inputcontainer}
                id="password"
                type="password"
                name="name"
                placeholder="Password"
                autoComplete=""
                onChange={(e) => setuserPassword(e.target.value)}
              />
            </div>
          </div>
          <FormLabel id="demo-row-radio-buttons-group-label">
            Login As:
          </FormLabel>
          <RadioGroup
            row
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
            onChange={(e) => {
              e.preventDefault();
              setloginAs(e.target.value);
            }}
          >
            <FormControlLabel
              value="Employee"
              control={<Radio />}
              label="Employee"
            />
            <FormControlLabel value="Admin" control={<Radio />} label="Admin" />
          </RadioGroup>
          <div className="text-end">
            <span className="text-primary">Forget password?</span>
          </div>

          <div>
            <button type="submit" className={styles.button}>
              Login
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
export default loginForm;

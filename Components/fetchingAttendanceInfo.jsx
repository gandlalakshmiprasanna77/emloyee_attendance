import { useSelector } from "react-redux";

const fetchingAttendanceInfo = (props) => {
  let mon = props.current_month;
  let year = props.current_year;
  let months = {
    0: "jan",
    1: "feb",
    2: "mar",
    3: "apr",
    4: "may",
    5: "jun",
    6: "july",
    7: "aug",
    8: "sep",
    9: "oct",
    10: "nov",
    11: "dec",
  };
  const attendancedata = useSelector((state) => state.admin.attendance);
  let month = months[mon];
  let currentdate = month + year;
  var dicti = attendancedata[currentdate];
  var days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
  let d = new Date(year, mon);
  var array = [];
  let row = 0;
  var col = 0;
  array[row] = [];

  for (let i = 0; i < getDay(d); i++) {
    array[row][col] = 0;
    col = col + 1;
  }
  while (d.getMonth() === mon) {
    let date = d.getDate();
    array[row][col] = date;
    if (getDay(d) % 7 == 6) {
      row += 1;
      array[row] = [];
      col = 0;
    } else {
      col = col + 1;
    }

    d.setDate(d.getDate() + 1);
  }
  if (getDay(d) != 0) {
    for (let i = getDay(d); i < 7; i++) {
      array[row][col] = 0;
      col = col + 1;
    }
  }
  function getDay(date) {
    let day = date.getDay();
    if (day == 0) day = 7;
    return day - 1;
  }
  return (
    <table className="w-100">
      <thead className="bg-white" style={{ border: "1px solid #cbd5e1" }}>
        <tr key="thead">
          {days.map((day) => {
            return (
              <td key={day}>
                <span className="text-secondary text-semi-bold"> {day} </span>
              </td>
            );
          })}
        </tr>
      </thead>
      <tbody style={{ border: "1px solid #cbd5e1" }}>
        {array.map((row, index) => {
          return (
            <tr style={{ border: "1px solid #cbd5e1" }} key={index}>
              {row.map((col, index) => {
                if (col !== 0 && index < 5) {
                  return (
                    <td style={{ border: "1px solid #cbd5e1" }} key={index}>
                      <div className="tablecell">
                        <div className="day">
                          <p style={{ fontWeight: "600", textAlign: "left" }}>
                            {col}
                          </p>
                        </div>

                        <div
                          style={{ textAlign: "center", marginBottom: "20px" }}
                        >
                          {typeof dicti !== "undefined" ? (
                            typeof dicti[col] !== "undefined" ? (
                              dicti[col].stringValue === "P" ? (
                                <span
                                  style={{
                                    fontSize: "16px",
                                    fontWeight: "600",
                                  }}
                                  className="text-primary"
                                >
                                  {dicti[col].stringValue}
                                </span>
                              ) : (
                                <span
                                  style={{
                                    fontSize: "14px",
                                    fontWeight: "600",
                                  }}
                                  className="text-danger"
                                >
                                  {dicti[col].stringValue}
                                </span>
                              )
                            ) : (
                              <></>
                            )
                          ) : (
                            <></>
                          )}
                        </div>
                      </div>
                    </td>
                  );
                } else if (col === 0) {
                  return (
                    <td
                      style={{
                        backgroundColor: "rgba(163,178,199,.15)",
                        border: "1px solid #cbd5e1",
                      }}
                      key={index}
                    ></td>
                  );
                } else {
                  return (
                    <td style={{ border: "1px solid #cbd5e1" }} key={index}>
                      <div
                        className="tablecell"
                        style={{
                          backgroundColor: "rgba(163,178,199,.15)",
                        }}
                      >
                        <div className="day">
                          <p style={{ fontWeight: "600", textAlign: "left" }}>
                            {col}
                          </p>
                        </div>
                        <div
                          style={{ textAlign: "center", marginBottom: "20px" }}
                        >
                          <span
                            style={{
                              backgroundColor: "rgba(163,178,199,.15)",
                            }}
                          >
                            Ho
                          </span>
                        </div>
                      </div>
                    </td>
                  );
                }
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};
export default fetchingAttendanceInfo;

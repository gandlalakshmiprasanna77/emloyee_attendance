import Navbar from "../../containers/navigations/Navbar";

const wrapper = (props) => {
  return (
    <div
      className="d-flex flex-column"
      style={{ width: "100%", height: "100%" }}
    >
      <div style={{ width: "100%" }}>
        <Navbar />
      </div>
      <main
        style={{
          margin: "auto",
          width: "100%",
          height: "100%",
        }}
      >
        {props.children}
      </main>
    </div>
  );
};
export default wrapper;

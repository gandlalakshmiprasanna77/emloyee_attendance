import { Chart as ChartJS } from "chart.js/auto";
import { Line } from "react-chartjs-2";
const lineChart = ({ chartData }) => {
  return <Line data={chartData} />;
};
export default lineChart;

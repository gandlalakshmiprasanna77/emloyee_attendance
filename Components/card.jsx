import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import styles from "../styles/card.module.scss";
const requestCard = (props) => {
  console.log(props.req_data);
  const {
    id,
    emp_name,
    leaveType,
    noOfDays,
    fromDate,
    toDate,
    fromSession,
    toSession,
    reason,
    appliedOn,
    name,
  } = props.req_data;

  return (
    <Card
      variant="outlined"
      sx={{ maxWidth: 800 }}
      style={{
        marginTop: "15px",
        marginLeft: "400px",
      }}
    >
      <CardContent>
        <Typography component="div" className={styles.card_typo}>
          <Typography component="div" className={styles.card_content}>
            <Typography component="div" className={styles.title}>
              Id
            </Typography>
            <Typography component="div" className={styles.data_font}>
              {id.stringValue}
            </Typography>
          </Typography>
          <Typography component="div" className={styles.card_content}>
            <Typography component="div" className={styles.title}>
              Name
            </Typography>
            <Typography component="div" className={styles.data_font}>
              {emp_name.stringValue}
            </Typography>
          </Typography>
          <Typography component="div" className={styles.card_content}>
            <Typography component="div" className={styles.title}>
              Leave Type
            </Typography>
            <Typography component="div" className={styles.data_font}>
              {leaveType.stringValue === "PL"
                ? "Previlege Leave"
                : leaveType.stringValue === "LOP"
                ? "Loss Of Pay"
                : "Bereavement Leave"}
            </Typography>
          </Typography>
          <Typography component="div" className={styles.card_content}>
            <Typography component="div" className={styles.title}>
              No.OfDays
            </Typography>
            <Typography component="div" className={styles.data_font}>
              {noOfDays.stringValue}
            </Typography>
          </Typography>
        </Typography>
        <Typography component="div" className={styles.card_content2}>
          <Typography component="div" className={styles.card_content3}>
            <Typography
              component="div"
              className={styles.title}
              style={{ width: "70px" }}
            >
              Duration:
            </Typography>
            <Typography className={styles.data_font}>
              {fromDate.stringValue} ( {fromSession.stringValue} ) to{" "}
              {toDate.stringValue} ( {toSession.stringValue} )
            </Typography>
          </Typography>
          <Typography component="div" className={styles.card_content3}>
            <Typography
              component="div"
              className={styles.title}
              style={{ width: "70px" }}
            >
              reason:
            </Typography>
            {reason.stringValue !== "" ? (
              <Typography className={styles.data_font}>
                {reason.stringValue}
              </Typography>
            ) : (
              <></>
            )}
          </Typography>
          <Typography component="div" className={styles.card_content3}>
            <Typography
              component="div"
              className={styles.title}
              style={{ width: "70px" }}
            >
              Applied On:
            </Typography>
            <Typography className={styles.data_font}>
              {appliedOn.stringValue}
            </Typography>
          </Typography>
        </Typography>
      </CardContent>
      <CardActions className={styles.action_area}>
        <Button
          size="small"
          className={styles.link}
          style={{ marginRight: "20px" }}
          onClick={() => {
            props.leaveSanctionedAction(name.stringValue);
          }}
        >
          Accept
        </Button>
        <Button
          size="small"
          className={styles.link}
          onClick={() => {
            props.leaveRejectedAction(name.stringValue);
          }}
        >
          Reject
        </Button>
      </CardActions>
    </Card>
  );
};
export default requestCard;

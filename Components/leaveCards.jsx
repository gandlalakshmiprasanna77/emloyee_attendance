import Card from "./card";
import { useState } from "react";
import styles from "../styles/leaveCards.module.scss";
import { useSelector } from "react-redux";
import { useEffect } from "react";
const leaveCards = () => {
  const [requests, setrequests] = useState([]);
  const employee_requests = useSelector((state) => state.admin.leave);
  useEffect(() => {
    const pending_requests = employee_requests.filter((request) => {
      return request.status.stringValue !== "Accepted";
    });
    setrequests(pending_requests);
  }, []);
  const months = [
    "jan",
    "feb",
    "mar",
    "apr",
    "may",
    "jun",
    "july",
    "aug",
    "sep",
    "oct",
    "nov",
    "dec",
  ];
  const leaveSanctionedAction = (name) => {
    const url = "https://firestore.googleapis.com/v1/" + name;
    const api = url + "?updateMask.fieldPaths=status";
    const data = { fields: { status: { stringValue: "Accepted" } } };
    const current_card_data = requests.filter((request) => {
      return request.name.stringValue === name;
    });
    const fromDate = current_card_data[0].fromDate.stringValue;
    const toDate = current_card_data[0].toDate.stringValue;
    const fromSession = current_card_data[0].fromSession.stringValue;
    const toSession = current_card_data[0].toSession.stringValue;
    const no_of_days = current_card_data[0].noOfDays.stringValue;
    const leaveType = current_card_data[0].leaveType.stringValue;
    const id = current_card_data[0].id.stringValue;
    const month_int = parseInt(fromDate.substring(5, 7));
    const month = months[month_int - 1];
    const year = fromDate.substring(0, 4);
    const date_field = month + year;
    const day = fromDate.substring(8, 10);
    const date = date_field;
    (async () => {
      try {
        var existed_data = await fetch(
          `https://firestore.googleapis.com/v1/projects/employee-attendance-90644/databases/(default)/documents/Attendance/${id}`
        );
        var existed_attendance = await existed_data.json();
        const attendance_document = existed_attendance.fields;
        const keys = Object.keys(attendance_document);
        const existing_structure = keys.filter((key) => {
          return key === date_field;
        });
        const existing_attendance_data = {};
        if (existing_structure.length !== 0) {
          existing_attendance_data =
            attendance_document[existing_structure[0]].mapValue.fields;
        }
        const days = parseInt(day);
        const year_int = parseInt(year);
        const days_for_month = new Date(year_int, month_int, 0).getDate();
        const whichday = new Date(
          year_int.toString() +
            "-" +
            month_int.toString() +
            "-" +
            days.toString()
        ).getDay();
        const totaldays = 0;
        const difference =
          (new Date(toDate) - new Date(fromDate)) / (1000 * 3600 * 24) + 1;
        const whichdays = whichday;
        for (let i = 1; i <= difference; i++) {
          if (whichdays !== 0 && whichdays !== 6) {
            totaldays++;
          }
          if (whichdays == 6) {
            whichdays = 0;
          } else {
            whichdays++;
          }
        }
        const i = 1;
        const body1 = {};
        const a = { mapValue: { fields: {} } };
        while (i <= totaldays) {
          if (whichday !== 0 && whichday !== 6) {
            if (i === 1) {
              if (fromSession === toSession && no_of_days === "0.5") {
                if (fromSession === "Session1") {
                  a.mapValue.fields[days] = { stringValue: leaveType + ":1" };
                } else {
                  a.mapValue.fields[days] = { stringValue: "1:" + leaveType };
                }
                body1[date_field] = a;
                break;
              } else if (totaldays === 1) {
                a.mapValue.fields[days] = { stringValue: leaveType };
                body1[date_field] = a;
                break;
              } else if (fromSession === "Session2") {
                a.mapValue.fields[days] = { stringValue: "1:" + leaveType };
              } else {
                a.mapValue.fields[days] = { stringValue: leaveType };
              }
            } else if (i === totaldays) {
              if (toSession === "Session1") {
                a.mapValue.fields[days] = { stringValue: leaveType + ":1" };
              } else {
                a.mapValue.fields[days] = { stringValue: leaveType };
              }
              body1[date_field] = a;
            } else {
              a.mapValue.fields[days] = { stringValue: leaveType };
            }
            i++;
          }
          days++;
          if (days > days_for_month) {
            body1[date_field] = a;
            a = { mapValue: { fields: {} } };
            days = 1;
            if (month_int === 12) {
              month_int = 1;
              year_int++;
            } else {
              month_int++;
            }
            month = months[month_int - 1];
            date_field = month + year_int;
            days_for_month = new Date(year_int, month_int, 0).getDate();
          }
          if (whichday === 6) {
            whichday = 0;
          } else {
            whichday++;
          }
        }
        const data_keys = Object.keys(existing_attendance_data);
        console.log(data_keys);
        for (let i = 0; i < data_keys.length; i++) {
          if (body1[date].mapValue.fields[data_keys[i]] === undefined) {
            body1[date].mapValue.fields[data_keys[i]] =
              existing_attendance_data[data_keys[i]];
          }
        }
        const body = {};
        body.fields = body1;
        console.log(body);
        const fields = Object.keys(body.fields);
        const update_url =
          "https://firestore.googleapis.com/v1/projects/employee-attendance-90644/databases/(default)/documents/Attendance/" +
          id +
          "?";
        for (let i = 0; i < fields.length; i++) {
          if (i === 0) {
            update_url += "updateMask.fieldPaths=" + fields[i];
          } else {
            update_url += "&updateMask.fieldPaths=" + fields[i];
          }
        }
        console.log(update_url);
        var updating = await fetch(update_url, {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(body),
        });
        const res = await updating.json();
        if (updating.status === 200) {
          var response = await fetch(api, {
            method: "PATCH",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          });

          if (response.status === 200) {
            const remainingrequests = requests.filter((request) => {
              return request.name.stringValue !== name;
            });
            setrequests(remainingrequests);
          }
        }
      } catch (error) {
        console.log(error);
      }
    })();
  };
  const leaveRejectedAction = (name) => {
    const url = "https://firestore.googleapis.com/v1/" + name;
    (async () => {
      try {
        const response = await fetch(url, {
          method: "DELETE",
        });
        if (response.status === 200) {
          const remainingrequests = requests.filter((request) => {
            return request.name.stringValue !== name;
          });
          setrequests(remainingrequests);
        }
      } catch (err) {
        console.log(err);
      }
    })();
  };
  return (
    <div className={styles.cards_group}>
      {requests.map((request) => {
        return (
          <Card
            key={request.name.stringValue}
            req_data={request}
            leaveSanctionedAction={leaveSanctionedAction}
            leaveRejectedAction={leaveRejectedAction}
          />
        );
      })}
    </div>
  );
};
export default leaveCards;

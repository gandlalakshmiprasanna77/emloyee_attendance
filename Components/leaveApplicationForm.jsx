import React, { useState } from "react";
import { useSelector } from "react-redux";

const leaveApplicationForm = () => {
  const employeedetails = useSelector((state) => state.employee.employees);
  const employeeId = useSelector((state) => state.employee.id);
  const employee_name = useSelector((state) => state.employee.name);
  const [phoneNumber, setphoneNumber] = useState("");
  const [reason, setreason] = useState("");
  const [leaveType, setleaveType] = useState("");
  const [fromDate, setfromDate] = useState("");
  const [fromSession, setfromSession] = useState("Session1");
  const [toDate, settoDate] = useState("");
  const [toSession, settoSession] = useState("Session2");
  const [gmail, setgmail] = useState("");
  const [added, setadded] = useState(false);
  const [noOfDays, setnoOfDays] = useState("");
  const [dateFormatError1, setdateFormatError1] = useState(false);
  const [dateFormatError2, setdateFormatError2] = useState(false);
  const [errMsg, seterrMsg] = useState("");
  const [err, seterr] = useState(false);
  const err_msg1 = "From Date should be less than To Date";
  const err_msg2 = "To date should be greater than From Date";
  const err_msg3 = "You dont need to apply leave  for this days ";
  const countingDays = (fromDate, toDate, fromSession, toSession, target) => {
    if (fromDate != "" && toDate !== "") {
      const difference =
        (new Date(toDate) - new Date(fromDate)) / (1000 * 3600 * 24) + 1;
      if (difference <= 0) {
        if (target === "fromDate") {
          setdateFormatError2(false);
          setdateFormatError1(true);
        } else if (target === "toDate") {
          setdateFormatError2(true);
          setdateFormatError1(false);
        }
      } else {
        console.log(difference);
        setdateFormatError2(false);
        setdateFormatError1(false);
        const day = new Date(fromDate).getDay();
        console.log(day);
        let totaldays = 0;
        let weekends;
        for (let i = 1; i <= difference; i++) {
          if (day !== 0 && day !== 6) {
            totaldays++;
          }
          if (day == 6) {
            day = 0;
          } else {
            day++;
          }
        }
        weekends = difference - totaldays;

        if (fromSession === toSession) {
          totaldays = totaldays - 0.5;
        } else if (fromSession === "Session2") {
          totaldays = totaldays - 1;
        }
        if (difference === weekends) {
          setnoOfDays("");
          seterr(true);
          seterrMsg(err_msg3);
          setTimeout(() => {
            seterr(false);
          }, 2000);
        } else if (totaldays === 0) {
          seterr(true);
          seterrMsg(err_msg1);
          setnoOfDays("");
          setTimeout(() => {
            seterr(false);
          }, 2000);
        } else {
          setnoOfDays(totaldays);
          seterrMsg("");
        }
      }
    }
  };
  const appliedOn = () => {
    var today = new Date();
    const todayDate =
      today.getFullYear() +
      "-" +
      (today.getMonth() + 1) +
      "-" +
      today.getDate();
    return todayDate;
  };
  const handlesubmit = (e) => {
    e.preventDefault();
    if (
      err === false &&
      dateFormatError1 === false &&
      dateFormatError2 === false &&
      errMsg === ""
    ) {
      const data = {
        fields: {
          id: {},
          fromDate: {},
          toDate: {},
          fromSession: {},
          toSession: {},
          leaveType: {},
          reason: {},
          phoneNumber: {},
          noOfDays: {},
          appliedOn: {},
          status: {},
          emp_name: {},
        },
      };
      let typeOfLeave;
      if (leaveType === "Previlege Leave") {
        typeOfLeave = "PL";
      } else if (leaveType === "Loss Of Pay") {
        typeOfLeave = "LOP";
      } else {
        typeOfLeave = "BL";
      }
      data.fields.id.stringValue = employeeId;
      data.fields.fromDate.stringValue = fromDate;
      data.fields.toDate.stringValue = toDate;
      data.fields.fromSession.stringValue = fromSession;
      data.fields.toSession.stringValue = toSession;
      data.fields.leaveType.stringValue = typeOfLeave;
      data.fields.appliedOn.stringValue = appliedOn();
      data.fields.noOfDays.stringValue = noOfDays.toString();
      data.fields.reason.stringValue = reason;
      data.fields.phoneNumber.stringValue = phoneNumber;
      data.fields.status.stringValue = "pending";
      data.fields.emp_name.stringValue = employee_name;
      console.log(JSON.stringify(data));
      (async () => {
        try {
          var response = await fetch(
            `https://firestore.googleapis.com/v1/projects/employee-attendance-90644/databases/(default)/documents/Leave/`,
            {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(data),
            }
          );
          if (response.status === 200) {
            setphoneNumber("");
            setreason("");
            setleaveType("");
            setfromDate("");
            setfromSession("Session1");
            settoDate("");
            settoSession("Session2");
            setnoOfDays("");
            setgmail("");
            setadded(true);
            setTimeout(() => {
              setadded(false);
            }, 2000);
          }
        } catch (error) {
          console.log(error);
        }
      })();
    }
  };
  const handleCancel = (e) => {
    e.preventDefault();
    setphoneNumber("");
    setreason("");
    setleaveType("");
    setfromDate("");
    setfromSession("");
    settoDate("");
    settoSession("");
    setgmail("");
    setnoOfDays("");
  };

  return (
    <React.Fragment>
      {err ? (
        <div
          className="mx-auto mt-1 bg-warning text-center"
          style={{ width: "400px" }}
        >
          {errMsg}
        </div>
      ) : (
        <></>
      )}
      <div
        className=" m-auto bg-white w-50 mt-5"
        style={{
          boxShadow: " 0 0 5px 0 rgba(0,0,0,0.2)",
          padding: "20px",
          fontFamily: "sans-serif",
        }}
      >
        {added ? (
          <div className="text-success fs-5 text-center mb-3">
            Successfully send request
          </div>
        ) : (
          <></>
        )}
        <div className="text-center text-primary mb-2">Applying for Leave</div>
        <form onSubmit={handlesubmit}>
          <label htmlFor="leavetype" className="text-secondary required-field">
            Leave type:
            <span className="required" style={{ color: "red" }}>
              *
            </span>
          </label>
          <select
            name="leavetype"
            id="leavetype"
            style={{
              marginLeft: "3rem",
              borderRadius: "0.5rem",
              height: "35px",
            }}
            value={leaveType}
            onChange={(e) => {
              setleaveType(e.target.value);
            }}
            required
          >
            <option value="" disabled hidden>
              Select type
            </option>
            <option value="Loss Of Pay">Loss Of Pay</option>
            <option value="Previlege Leave">Previlege Leave</option>
            <option value="Bereavement Leave">Bereavement Leave</option>
          </select>
          <div className="d-flex flex-row mt-2">
            <div>
              <div style={{ width: "100px" }}>
                <label htmlFor="date" className="text-secondary required-field">
                  From Date:
                  <span className="required" style={{ color: "red" }}>
                    *
                  </span>
                </label>
              </div>
              <div>
                <input
                  type="date"
                  id="date"
                  className="text-secondary"
                  value={fromDate}
                  onChange={(e) => {
                    countingDays(
                      e.target.value,
                      toDate,
                      fromSession,
                      toSession,
                      "fromDate"
                    );
                    setfromDate(e.target.value);
                  }}
                  required
                />
              </div>
            </div>
            <div style={{ marginLeft: "5rem", width: "5rem" }}>
              <label
                htmlFor="fromsession"
                className="text-secondary required-field"
              >
                Session:
              </label>

              <div>
                <select
                  name="leavetype"
                  id="fromsession"
                  style={{ width: "200px", height: "30px" }}
                  value={fromSession}
                  onChange={(e) => {
                    countingDays(
                      fromDate,
                      toDate,
                      e.target.value,
                      toSession,
                      "fromSession"
                    );
                    setfromSession(e.target.value);
                  }}
                >
                  <option value="Session1">Session1</option>
                  <option value="Session2">Session2</option>
                </select>
              </div>
            </div>
          </div>
          {dateFormatError1 ? (
            <div
              style={{ fontSize: "12px", marginTop: "3px" }}
              className="text-danger"
            >
              {err_msg1}
            </div>
          ) : (
            <></>
          )}
          <div className="d-flex flex-row mt-3">
            <div>
              <div style={{ width: "100px" }}>
                <label
                  htmlFor="todate"
                  className="text-secondary required-field"
                >
                  To Date:
                  <span className="required" style={{ color: "red" }}>
                    *
                  </span>
                </label>
              </div>
              <div>
                <input
                  type="date"
                  id="todate"
                  className="text-secondary"
                  value={toDate}
                  onChange={(e) => {
                    countingDays(
                      fromDate,
                      e.target.value,
                      fromSession,
                      toSession,
                      "toDate"
                    );
                    settoDate(e.target.value);
                  }}
                  required
                />
              </div>
            </div>
            <div style={{ marginLeft: "5rem", width: "5rem" }}>
              <label
                htmlFor="tosession"
                className="text-secondary required-field"
              >
                Session:
              </label>
              <div>
                <select
                  name="leavetype"
                  id="tosession"
                  style={{ width: "200px", height: "30px" }}
                  onChange={(e) => {
                    countingDays(
                      fromDate,
                      toDate,
                      fromSession,
                      e.target.value,
                      "toSession"
                    );
                    settoSession(e.target.value);
                  }}
                  value={toSession}
                >
                  <option value="Session1">Session1</option>
                  <option value="Session2">Session2</option>
                </select>
              </div>
            </div>
            <div style={{ marginLeft: "15rem" }} className="text-secondary">
              Applying for :<span>{noOfDays}</span>
            </div>
          </div>
          {dateFormatError2 ? (
            <div
              style={{ fontSize: "12px", marginTop: "5px" }}
              className="text-danger"
            >
              {err_msg2}
            </div>
          ) : (
            <></>
          )}
          <div className="mt-3">
            <label htmlFor="gmail" className="text-secondary required-field">
              Applying to:
              <span className="required" style={{ color: "red" }}>
                *
              </span>
            </label>
            <div>
              <select
                name="gmail"
                id="gmail"
                style={{ width: "200px", height: "30px" }}
                value={gmail}
                onChange={(e) => setgmail(e.target.value)}
                required
              >
                <option value="" disabled hidden>
                  Select mentor
                </option>
                {employeedetails.map((em) => {
                  return (
                    <option value={em.id} key={em.id}>
                      {em.name}#{em.id}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
          <div className="mt-2">
            <label htmlFor="phonenum" className="text-secondary">
              Contact Details
            </label>
            <div>
              <input
                id="phonenum"
                type="text"
                value={phoneNumber}
                placeholder="phone number"
                onChange={(e) => setphoneNumber(e.target.value)}
              />
            </div>
          </div>
          <div className="mt-2">
            <label htmlFor="reason" className="text-secondary">
              Reason
            </label>
            <div>
              <textarea
                id="reason"
                className="text-secondary"
                cols="80"
                rows="2"
                type="text"
                placeholder="enter reason"
                value={reason}
                onChange={(e) => setreason(e.target.value)}
              />
            </div>
          </div>
          <div
            className="d-flex flex-row mt-3 "
            style={{ marginLeft: "10rem" }}
          >
            <button
              type="submit"
              className="btn btn-sm btn-primary"
              style={{ width: "120px" }}
            >
              Submit
            </button>
            <button
              type="cancel"
              className="btn text-primary"
              style={{
                marginLeft: "30px",
                width: "120px",
                border: "1px solid dodgerblue",
              }}
              onClick={handleCancel}
            >
              cancel
            </button>
          </div>
        </form>
      </div>
    </React.Fragment>
  );
};
export default leaveApplicationForm;
